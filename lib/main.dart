

import 'package:flutter/material.dart';

void main() => runApp(MyApp());


class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage();
  List<String> names = [
    'Carlos',
    'Gael',
    'Zero',
    'Kallen',
    'Ania',
    'Lancelot',
    'Zero',
    'Kallen',
    'Ania',
    'Lancelot',
    'Bismark',
    'Gino',
    'Luciano',
    'Bill',
    'goku',
    'vegueta'];
  @override 
  Widget build(BuildContext context){
    return Scaffold(
      
      appBar: AppBar(),
      body: ListView.builder(
        itemCount: names.length,
        itemBuilder: (BuildContext context, int index){
          final name = names[index];
          return ListTile(
            title: Text(name),
            leading: Icon(Icons.person),
            onTap: (){
              print(name);
            },
          );
        }
        )
    );
  }
  
}

